use std::path::Path;
use std::io;
use std::fs;

/// Returns a list of files in a directory
///
/// # Arguments
///
/// * `path` - A string of the path to start
///
/// # Errors
/// This function will return an error in the following situations, but is not limited to just these cases:
///
/// * The provided path doesn't exist.
/// * The process lacks permissions to view the contents.
/// * The path points at a non-directory file.
///
/// # Example
///
/// ```
/// extern crate crawler;
///
/// use std::path::Path;
///
/// fn main() {
///     let path = String::from(".");
///     let result = crawler::crawl(&path);
/// }
/// ```
///
pub fn crawl(path: &String) -> Result<Vec<String>, io::Error> {
    let path = Path::new(&path);

    let mut files = Vec::new();

    list_files(&mut files, path)?;

    Ok(files)
}

fn list_files(files: &mut Vec<String>, path: &Path) -> Result<(), io::Error> {
    for entry in fs::read_dir(&path)? {
        let entry = entry?;
        let new_path = entry.path();

        if new_path.is_dir() {
            list_files(files, &new_path)?;
        } else {
            files.push(String::from(new_path.to_str().unwrap()));
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
