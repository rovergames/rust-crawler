extern crate crawler;

use std::env;

fn main() {
    let mut args = env::args();
    args.next();

    let path = match args.next() {
        Some(path) => path,
        None => panic!("Could not find the path!"),
    };
    let result = crawler::crawl(&path);

    println!("{:?}", result);
}
